package main

import (
	"fmt"

	pa "gitlab.utc.fr/wilsonba/ia04td2abonus/agt/playeragent"
)

func main() {
	ag := pa.NewPlayerAgent("id1", "http://localhost:8080", "optimal", 0, 1000)
	ag.Start()
	fmt.Scanln()
}
