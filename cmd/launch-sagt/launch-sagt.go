package main

import (
	"fmt"

	sa "gitlab.utc.fr/wilsonba/ia04td2abonus/agt/serveragent"
)

func main() {
	server := sa.NewServerAgent(":8080")
	server.Start()
	fmt.Scanln()
}
