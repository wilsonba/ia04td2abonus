# Serveur de Plus petit/plus grand Rest en Go

Le serveur permet de jouer à "Plus petit/plus grand" (le juste prix).

Les joueurs font des requêtes au serveur pour trouver le nombre choisi.

Toutes les requêtes doivent suivre la norme suivante :

2 exécutables (indépendants) sont fournis :

* `launch-sagt` permet de lancer un agent de type serveur
* `launch-pagt` permet de lancer un agent de type joueur

Il est possible d'avoir accès à ces commandes en les installant ainsi :

`go install gitlab.utc.fr/wilsonba/ia04td2abonus/cmd/launch-sagt`

Pour jouer, il suffit de demander une nouvelle partie par requête POST sur localhost:8080/new_game avec pour argument bounds, une slice de deux entiers qui délimite l'espace de jeu.

Pour deviner le bon nombre, il faut envoyer des suppositions par requête POST sur localhost:8080/play avec pour arguments 
* game-id : une chaîne de caractères qui identifie la partie (elle a été donnée losr de la requête de création) 
* guess : un entier

Le serveur renvoie ensuite une indication sur le nombre à trouver.

Il est possible d'instancier un agent joueur qui peut jouer automatiquement de manière optimale ou aléatoire.
