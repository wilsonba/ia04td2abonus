package playeragent

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"sort"
	"strconv"

	rad "gitlab.utc.fr/wilsonba/ia04td2abonus"
)

type PlayerAgent struct {
	id        string
	url       string
	strategy  string
	game      rad.GameForPlayer
	hasWon    bool
	nbGuesses int
}

func NewPlayerAgent(id string, url string, strat string, lowerLimit int, upperLimit int) *PlayerAgent {
	return &PlayerAgent{
		id,
		url,
		strat,
		rad.GameForPlayer{Bounds: []int{lowerLimit, upperLimit}},
		false,
		0,
	}
}

func (pa *PlayerAgent) treatResponse(r *http.Response, err error, gameCreation bool) (res rad.GameForPlayer) {
	// traitement de la réponse
	if err != nil {
		return
	}
	if r.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", r.StatusCode, r.Status)
		return
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	if !gameCreation {
		newBounds := pa.game.Bounds

		switch {
		case bytes.Contains(buf.Bytes(), []byte("correct!")):
			pa.hasWon = true
			return
		case bytes.Contains(buf.Bytes(), []byte("is too")):
			guess, indication, _ := bytes.Cut(buf.Bytes(), []byte(" is too "))
			guessInt, _ := strconv.Atoi(string(guess[:]))
			if bytes.Contains(indication, []byte("higher")) {
				newBounds = []int{guessInt, newBounds[1]}
			} else {
				newBounds = []int{newBounds[0], guessInt}
			}
		}
		res = rad.GameForPlayer{GameID: pa.game.GameID, Bounds: newBounds}

		return
	}

	json.Unmarshal(buf.Bytes(), &res)

	return
}

func (pa *PlayerAgent) requestGame() (bounds []int, err error) {
	// sérialisation de la requête
	url := pa.url + "/new_game"

	// création de la plage de valeurs
	bounds = pa.game.Bounds
	sort.Ints(bounds)
	req := rad.GameRequest{Bounds: bounds}

	// envoi de la requête
	data, _ := json.Marshal(req)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	game := pa.treatResponse(resp, err, true)
	pa.game = game

	return
}

func (pa *PlayerAgent) play() (secret int, err error) {
	// sérialisation de la requête
	url := pa.url + "/play"
	guess := 0
	bounds := pa.game.Bounds
	pa.nbGuesses++

	switch pa.strategy {
	case "optimal":
		guess = (bounds[0] + bounds[1]) / 2
	default:
		guess = bounds[0] + rand.Intn(bounds[1]-bounds[0])
	}

	req := rad.Guess{
		GameID: pa.game.GameID,
		Guess:  guess,
	}

	data, _ := json.Marshal(req)

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	game := pa.treatResponse(resp, err, false)

	if pa.hasWon {
		return guess, err
	}

	pa.game = game
	secret, err2 := pa.play()

	return secret, errors.Join(err, err2)
}

func (pa *PlayerAgent) Start() {
	log.Printf("démarrage de %s", pa.id)
	sBounds, err := pa.requestGame()

	secret, err2 := pa.play()

	err = errors.Join(err, err2)

	if err != nil {
		log.Fatal(pa.id, "error:", err.Error())
	} else {
		log.Printf("%s found the secret number : %d, in %d guesses (bounds: [%d, %d])", pa.id, secret, pa.nbGuesses, sBounds[0], sBounds[1])
	}
}
