package serveragent

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"

	rad "gitlab.utc.fr/wilsonba/ia04td2abonus" // à remplacer par le nom du dossier actuel
)

func contains(s map[string]rad.GameForServer, e string) bool {
	for a, _ := range s {
		if a == e {
			return true
		}
	}
	return false
}

type GameServerAgent struct {
	sync.Mutex
	id    string
	addr  string
	games map[string]rad.GameForServer
}

func NewServerAgent(addr string) *GameServerAgent {
	return &GameServerAgent{id: addr, addr: addr}
}

// Test de la méthode
func (gsa *GameServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func decodeRequest[Req rad.Guess | rad.GameRequest](r *http.Request) (req Req, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (gsa *GameServerAgent) createNewGame(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	gsa.Lock()
	defer gsa.Unlock()

	// vérification de la méthode de la requête
	if !gsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := decodeRequest[rad.GameRequest](r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	gameId := fmt.Sprintf("game-%d", len(gsa.games)+1)
	bounds := req.Bounds

	// vérification de la plage de valeurs
	if bounds[0] >= bounds[1] {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, errors.New("the bounds are not valid"))
		return
	}

	toFind := bounds[0] + rand.Intn(bounds[1]-bounds[0])

	w.WriteHeader(http.StatusOK)
	newGame := rad.GameForServer{
		Bounds:  bounds,
		Number:  toFind,
		Guesses: 0,
		IsOpen:  true,
	}
	gsa.games[gameId] = newGame
	serial, _ := json.Marshal(rad.GameForPlayer{GameID: gameId, Bounds: bounds})
	w.Write(serial)
}

func (gsa *GameServerAgent) receiveGuess(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	gsa.Lock()
	defer gsa.Unlock()

	// vérification de la méthode de la requête
	if !gsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := decodeRequest[rad.Guess](r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// recupération des infos
	game := req.GameID

	// Check if game exists
	if !contains(gsa.games, game) {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("The game '%s' does not exist", game)
		w.Write([]byte(msg))
		return
	}

	serverGame := gsa.games[game]

	// Check que la partie n'est pas déjà finie
	if !serverGame.IsOpen {
		w.WriteHeader(http.StatusServiceUnavailable)
		msg := fmt.Sprintf("The game %s is already finished, the answer was %d.", game, serverGame.Number)
		w.Write([]byte(msg))
		return
	}

	nbGuesses := serverGame.Guesses + 1
	isOpen := true

	w.WriteHeader(http.StatusOK)
	msg := ""

	switch {
	case req.Guess == serverGame.Number:
		isOpen = false
		msg = fmt.Sprintf("Your guess is correct! %d was the secret number, you found it in %d guesses.", req.Guess, nbGuesses)
	case req.Guess < serverGame.Number:
		msg = fmt.Sprintf("%d is too low. Go higher!\nYou have made %d guesses.", req.Guess, nbGuesses)
	case req.Guess > serverGame.Number:
		msg = fmt.Sprintf("%d is too high. Go lower!\nYou have made %d guesses.", req.Guess, nbGuesses)
	}

	w.Write([]byte(msg))

	gsa.games[game] = rad.GameForServer{
		Bounds:  serverGame.Bounds,
		Number:  serverGame.Number,
		Guesses: nbGuesses,
		IsOpen:  isOpen,
	}
}

func (gsa *GameServerAgent) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/new_game", gsa.createNewGame)
	mux.HandleFunc("/play", gsa.receiveGuess)

	gsa.games = make(map[string]rad.GameForServer)

	// création du serveur http
	s := &http.Server{
		Addr:           gsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", gsa.addr)
	go log.Fatal(s.ListenAndServe())
}
