package ia04td2abonus

type Guess struct {
	GameID string `json:"game-id"`
	Guess  int    `json:"guess"`
}

type GameRequest struct {
	Bounds []int `json:"bounds"`
}

type GameForPlayer struct {
	GameID string `json:"game-id"`
	Bounds []int  `json:"bounds"`
}

type GameForServer struct {
	Bounds  []int
	Number  int
	Guesses int
	IsOpen  bool
}
